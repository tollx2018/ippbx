import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Home from "./screens/Home/Home";
import Accounts from "./screens/Accounts/Accounts";
import UserSettings from "./screens/UserSettings";
import New from "./screens/New";
import Login from "./screens/SignIn";
import VeryfyEmail from './screens/SignUp/VeryfyEmail';
import SignUp from "./screens/SignUp";
import ForgotPassword from "./screens/ForgotPassword";
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import * as serviceWorker from "./serviceWorker";

ReactDOM.render(
  <div>
    <BrowserRouter>
      <Switch>
        <Route exact path={"/login"} component={Login} name="Login" />
        <Route exact path={"/register"} component={SignUp} name="SignUp" />
        <Route exact path={"/verify_email"} component={VeryfyEmail} name="VerifyEmail" />
        <Route exact path={"/forgot"} component={ForgotPassword} name="ForgotPassword" />
        <Route exact path={"/"} component={Home} name="Home"/>
        <Route exact path={"/accounts_users"} component={Accounts} name="Accounts"/>
        <Route exact path={"/user_settings"} component={UserSettings} name="UserSettings"/>
        <Route exact path={"/new"} component={New} name="New"/>
      </Switch>
    </BrowserRouter>
  </div>,
  document.getElementById("root")
);

serviceWorker.unregister();
