import React, {useEffect} from "react";
import {Link} from 'react-router-dom';
import {changeTheme} from "../utils";

const SignIn = props => {
  useEffect(() => {
    changeTheme();
  });
  return (
    <div id="authentication" style={{marginTop: 80}}>
      <div id="content" className="container-fluid">
        <a href="https://www.calltrackingmetrics.com/" id="logo" style={{maxWidth: '100%'}}>CallTrackingMetrics</a>
        <div id="authbox">
          <div className="new_user" id="new_user">
            <div id="signin">
              <input type="email" placeholder="Email Address" className="text" autoComplete="email" id="user_login"/>
              <input placeholder="Password" className="text" autoComplete="current-password" type="password"
                     id="user_password"/>

              <Link className="button callout bigbutton" to={"/"}>
                Sign In
              </Link>
              <div id="forgot">
                <Link to={"/forgot"}>Forgot your password?</Link>
                <Link to={"/register"}>Sign up</Link>
                <div className="clear"/>
              </div>
            </div>

          </div>
        </div>
        <div className="clear"/>
      </div>
    </div>
  );
}

export default SignIn;
