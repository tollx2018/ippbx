import React, {Component} from "react";

class ForgotPassword extends Component {
  render() {
    return (
      <div id="authentication" style={{marginTop: 80}}>
        <div id="content" className="container-fluid">
          <a href="#" id="logo" style={{maxWidth: '100%'}}>CallTrackingMetrics</a>
          <div id="authbox">
            <form className="new_user" id="new_user">
              <div id="signin">
                {/*<h3>Forgot your password?</h3>*/}
                <h4 style={{lineHeight: '140%'}}>
                  Enter your email address below to receive instructions for resetting your password:
                </h4>
                <input type="email" placeholder="Email Address" className="text" id="user_email"/>
                <input type="submit" name="commit" value="Send password instructions"
                       className="button callout bigbutton"/>
                <div id="forgot">
                  <a href="#">Sign in</a>
                  <a href="#">Sign up</a>
                  <div className="clear"/>
                </div>
              </div>
            </form>
          </div>
          <div className="clear"/>
        </div>
      </div>
    );
  }
}

export default ForgotPassword;
