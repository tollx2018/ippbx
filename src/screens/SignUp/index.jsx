import React, {useEffect} from "react";
import './signup.css';
import './signup_wizard_new.css';
import {changeTheme} from "../../utils";

const SignUp = props => {
  useEffect(() => {
    changeTheme();
  });
  return (
    <div>
      <div id="sign-up-wrapper">
        <div className="sign-up-window">
          <div className="sign-up-card">
            <header className="custom_header">
              <img className="ctm-logo" alt="logo" src="https://dwklcmio8m2n2.cloudfront.net/images/logo2x2.png"/>
            </header>
            <div className="sign-up-content">
              <div style={{margin: 'auto'}}>
                <div id="plan-form">
                  <div className="container-fluid">
                    <p style={{paddingLeft: 5, marginTop: 20, marginBottom: 15, color: 'black'}}>
                      Select a plan to get started.
                    </p>
                  </div>
                  <div className="container-fluid">
                    <div className="col-md-4 col-xs-12 col-sm-12">
                      <div className="plan-card" data-type="business" data-price="39">
                        <h3>Business</h3>
                        <span className="desc">Great for Small Businesses</span>
                        <span className="price">$39/mo <small>+ usage</small></span>
                        <button className="button" onClick={() => props.history.push("/verify_email")}>Select</button>
                      </div>
                    </div>
                    <div className="col-md-4 col-xs-12 col-sm-12">
                      <div className="plan-card" data-type="marketing">
                        <h3>Marketing</h3>
                        <span className="desc">Great for Marketing &amp; Advertising Agencies</span>
                        <span className="price">$99/mo <small>+ usage</small></span>
                        <button className="button" onClick={() => props.history.push("/verify_email")}>Select</button>
                      </div>
                    </div>
                    <div className="col-md-4 col-xs-12 col-sm-12">
                      <div className="plan-card" data-type="contact">
                        <h3>Contact Center</h3>
                        <span className="desc">Great for Contact Centers &amp; Lead Sellers</span>
                        <span className="price">$299/mo <small>+ usage</small></span>
                        <button className="button" onClick={() => props.history.push("/verify_email")}>Select</button>
                      </div>
                    </div>
                  </div>
                  <div className="container-fluid" style={{paddingTop: 10}}>
                    <p style={{paddingLeft: 5}}><a href="#" target="_blank">Learn more about the plans </a></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SignUp;
