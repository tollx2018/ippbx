import React, {useState} from "react";
import {AnimateOnChange} from 'react-animation';
import Step from "./Components/Step";
import './signup_wizard_new.css';
import './signup.css';
import Verify from "./Components/Verify";
import Confirm from "./Components/Confirm";
import Profile from "./Components/Profile";
import Checkout from "./Components/Checkout";


const VeryfyEmail = props => {
  const [index, setIndex] = useState(0);
  return (
    <div style={{color: '#222'}}>
      {/*<img alt="logo"*/}
           {/*src="https://dv36c15u2wg3n.cloudfront.net/assets/userflow_bg-a92709c76d60e45d781f58c1921dce89.svg"*/}
           {/*style={{width: '100%', margin: 0, position: 'fixed', top: -90}}/>*/}
      <div id="sign-up-wrapper" className="step-1">
        <div className="sign-up-window">
          <div className="sign-up-card">
            <header>
              <img className="ctm-logo" alt="logo" src="https://dwklcmio8m2n2.cloudfront.net/images/logo2x2.png"/>
            </header>
            <Step index={index}/>
            <div className="sign-up-content">
              <AnimateOnChange style={{width: 840}}>
                {index === 0 && <Verify/>}
                {index === 1 && <Confirm/>}
                {index === 2 && <Profile/>}
                {index === 3 && <Checkout/>}
              </AnimateOnChange>
              <div className="details-right checkout-summary">
                <div id="signup_summary">
                  <h3 style={{color: '#222'}}>Summary</h3>
                  <hr/>
                  <div className="summary-info">
                    <div className="item-cost-split">
                      <select className="select-plan" title="Agent Assignment" style={{color: 'black'}}>
                        <option selected="" value="business">Business $39/mo</option>
                        <option value="marketing">Marketing $99/mo</option>
                        <option value="contact">Contact Center $299/mo</option>
                      </select>
                    </div>
                    <div className="item-cost-split">
                      <p>Monthly Plan Fee</p>
                      <p id="selected-plan-cost">$39.00</p>
                    </div>

                    <div className="item-cost-split">
                      <p>Starting Available Balance</p>
                      <select className="starting-balance" name="" style={{color: 'black'}}>
                        <option value="3500">$35.00</option>
                        <option value="4000">$40.00</option>
                        <option value="4500">$45.00</option>
                        <option value="5000">$50.00</option>
                        <option value="5500">$55.00</option>
                        <option value="6000">$60.00</option>
                        <option value="6500">$65.00</option>
                        <option value="7000">$70.00</option>
                        <option value="7500">$75.00</option>
                        <option value="8000">$80.00</option>
                        <option value="8500">$85.00</option>
                        <option value="9000">$90.00</option>
                        <option value="9500">$95.00</option>
                        <option value="10000">$100.00</option>
                        <option value="10500">$105.00</option>
                        <option value="11000">$110.00</option>
                        <option value="11500">$115.00</option>
                        <option value="12000">$120.00</option>
                        <option value="12500">$125.00</option>
                        <option value="13000">$130.00</option>
                        <option value="13500">$135.00</option>
                        <option value="14000">$140.00</option>
                        <option value="14500">$145.00</option>
                        <option value="15000">$150.00</option>
                        <option value="15500">$155.00</option>
                        <option value="16000">$160.00</option>
                        <option value="16500">$165.00</option>
                        <option value="17000">$170.00</option>
                        <option value="17500">$175.00</option>
                        <option value="18000">$180.00</option>
                        <option value="18500">$185.00</option>
                        <option value="19000">$190.00</option>
                        <option value="19500">$195.00</option>
                        <option value="20000">$200.00</option>
                        <option value="20500">$205.00</option>
                        <option value="21000">$210.00</option>
                        <option value="21500">$215.00</option>
                        <option value="22000">$220.00</option>
                        <option value="22500">$225.00</option>
                        <option value="23000">$230.00</option>
                        <option value="23500">$235.00</option>
                        <option value="24000">$240.00</option>
                        <option value="24500">$245.00</option>
                        <option value="25000">$250.00</option>
                        <option value="26000">$260.00</option>
                        <option value="27000">$270.00</option>
                        <option value="28000">$280.00</option>
                        <option value="29000">$290.00</option>
                        <option value="30000">$300.00</option>
                        <option value="31000">$310.00</option>
                        <option value="32000">$320.00</option>
                        <option value="33000">$330.00</option>
                        <option value="34000">$340.00</option>
                        <option value="35000">$350.00</option>
                        <option value="36000">$360.00</option>
                        <option value="37000">$370.00</option>
                        <option value="38000">$380.00</option>
                        <option value="39000">$390.00</option>
                        <option value="40000">$400.00</option>
                        <option value="41000">$410.00</option>
                        <option value="42000">$420.00</option>
                        <option value="43000">$430.00</option>
                        <option value="44000">$440.00</option>
                        <option value="45000">$450.00</option>
                        <option value="46000">$460.00</option>
                        <option value="47000">$470.00</option>
                        <option value="48000">$480.00</option>
                        <option value="49000">$490.00</option>
                        <option value="50000">$500.00</option>
                        <option value="52500">$525.00</option>
                        <option value="55000">$550.00</option>
                        <option value="57500">$575.00</option>
                        <option value="60000">$600.00</option>
                        <option value="62500">$625.00</option>
                        <option value="65000">$650.00</option>
                        <option value="67500">$675.00</option>
                        <option value="70000">$700.00</option>
                        <option value="72500">$725.00</option>
                        <option value="75000">$750.00</option>
                        <option value="77500">$775.00</option>
                        <option value="80000">$800.00</option>
                        <option value="82500">$825.00</option>
                        <option value="85000">$850.00</option>
                        <option value="87500">$875.00</option>
                        <option value="90000">$900.00</option>
                        <option value="92500">$925.00</option>
                        <option value="95000">$950.00</option>
                        <option value="97500">$975.00</option>
                        <option value="100000">$1,000.00</option>
                        <option value="110000">$1,100.00</option>
                        <option value="120000">$1,200.00</option>
                        <option value="130000">$1,300.00</option>
                        <option value="140000">$1,400.00</option>
                        <option value="150000">$1,500.00</option>
                        <option value="160000">$1,600.00</option>
                        <option value="170000">$1,700.00</option>
                        <option value="180000">$1,800.00</option>
                        <option value="190000">$1,900.00</option>
                        <option value="200000">$2,000.00</option>
                        <option value="225000">$2,250.00</option>
                        <option value="250000">$2,500.00</option>
                        <option value="275000">$2,750.00</option>
                        <option value="300000">$3,000.00</option>
                        <option value="325000">$3,250.00</option>
                        <option value="350000">$3,500.00</option>
                        <option value="375000">$3,750.00</option>
                        <option value="400000">$4,000.00</option>
                        <option value="425000">$4,250.00</option>
                        <option value="450000">$4,500.00</option>
                        <option value="475000">$4,750.00</option>
                        <option value="500000">$5,000.00</option>
                        <option value="525000">$5,250.00</option>
                        <option value="550000">$5,500.00</option>
                        <option value="575000">$5,750.00</option>
                        <option value="600000">$6,000.00</option>
                        <option value="625000">$6,250.00</option>
                        <option value="650000">$6,500.00</option>
                        <option value="675000">$6,750.00</option>
                        <option value="700000">$7,000.00</option>
                        <option value="725000">$7,250.00</option>
                        <option value="750000">$7,500.00</option>
                        <option value="775000">$7,750.00</option>
                        <option value="800000">$8,000.00</option>
                        <option value="825000">$8,250.00</option>
                        <option value="850000">$8,500.00</option>
                        <option value="875000">$8,750.00</option>
                        <option value="900000">$9,000.00</option>
                        <option value="925000">$9,250.00</option>
                        <option value="950000">$9,500.00</option>
                        <option value="975000">$9,750.00</option>
                        <option value="1000000">$10,000.00</option>
                        <option value="1100000">$11,000.00</option>
                        <option value="1200000">$12,000.00</option>
                        <option value="1300000">$13,000.00</option>
                        <option value="1400000">$14,000.00</option>
                        <option value="1500000">$15,000.00</option>
                        <option value="1600000">$16,000.00</option>
                        <option value="1700000">$17,000.00</option>
                        <option value="1800000">$18,000.00</option>
                        <option value="1900000">$19,000.00</option>
                        <option value="2000000">$20,000.00</option>
                        <option value="2100000">$21,000.00</option>
                        <option value="2200000">$22,000.00</option>
                        <option value="2300000">$23,000.00</option>
                        <option value="2400000">$24,000.00</option>
                        <option value="2500000">$25,000.00</option>
                        <option value="2600000">$26,000.00</option>
                        <option value="2700000">$27,000.00</option>
                        <option value="2800000">$28,000.00</option>
                        <option value="2900000">$29,000.00</option>
                        <option value="3000000">$30,000.00</option>
                        <option value="3100000">$31,000.00</option>
                        <option value="3200000">$32,000.00</option>
                        <option value="3300000">$33,000.00</option>
                        <option value="3400000">$34,000.00</option>
                        <option value="3500000">$35,000.00</option>
                        <option value="3600000">$36,000.00</option>
                        <option value="3700000">$37,000.00</option>
                        <option value="3800000">$38,000.00</option>
                        <option value="3900000">$39,000.00</option>
                        <option value="4000000">$40,000.00</option>
                        <option value="4100000">$41,000.00</option>
                        <option value="4200000">$42,000.00</option>
                        <option value="4300000">$43,000.00</option>
                        <option value="4400000">$44,000.00</option>
                        <option value="4500000">$45,000.00</option>
                        <option value="4600000">$46,000.00</option>
                        <option value="4700000">$47,000.00</option>
                        <option value="4800000">$48,000.00</option>
                        <option value="4900000">$49,000.00</option>
                        <option value="5000000">$50,000.00</option>
                      </select>
                    </div>
                    <div className="item-cost-split">
                      <div/>
                      <p><a target="_blank" href="#">Learn more <i className="font-external-link"/></a></p>
                    </div>

                    <hr/>
                    <div className="item-cost-split">
                      <p>Charged Today</p>
                      <p className="charge-today">$74.00</p>
                    </div>
                  </div>
                </div>

                <footer id="wizard-buttons">
                  {index === 3 && <button id="prev" className="button outlined prev" onClick={() => setIndex(2)}>Prev</button>}
                  <button type="submit" className="callout button continue g-recaptcha" onClick={() => {
                    console.log(index);
                    if (index < 3) {
                      setIndex(index + 1)
                    }
                  }}>{index === 0 ? "Continue" : index === 3 ? "Complete Signup" :  'Next'}
                  </button>
                </footer>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default VeryfyEmail;
