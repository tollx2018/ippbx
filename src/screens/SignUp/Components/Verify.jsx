import React from 'react';

const Verify = props => {
  return (
    <div id="wizard-step" className="details-left">
      <div className="signup_section">
        <div id="email-signup" className="new_signup">
          <h2 style={{color: '#222'}}>First things first</h2>
          <p>Let's get started by verifying your email address.</p>
          <div className="textfield" style={{width: '60%'}}>
            <input className="text string" placeholder="Enter email address" type="text"
                   id="signup_user_email" style={{color: '#222'}}/>
            <div className="clear-break"/>
          </div>
          <span className="hint" style={{color: '#222'}}>
                        You will receive an email shortly with a code required for the next step.
                      </span>
          <div className="why-text">
            <p>By creating a CallTrackingMetrics account, you're agreeing to accept the
              CallTrackingMetrics&nbsp;
              <a href="#" target="_blank">terms of service</a>.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
};

export default Verify;
