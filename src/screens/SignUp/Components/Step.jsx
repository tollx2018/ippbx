import React from 'react';
import ReactTooltip from 'react-tooltip';

const Step = props => {
  const {index} = props;
  return (
    <div className="timeline-scroll">
      <div className="sign-up-timeline">
        <div className={`su-step verify-email ${index === 0 ? "active" : "complete"}`}>
          <i className="su-icon icon-secured-letter"/>
          <i className="su-icon icon-checkmark"/>
          <div className="su-timeline-number">1</div>
          <div className="step-label">Verify Email</div>
          <div className="horz-line end"/>
        </div>
        <div className={`check-inbox ${index < 1 ? "" : "complete"}`}>
          <div className="check-inbox-text">
            <p>Check inbox<span data-tip="A confirmation code was sent to your email"><i className="font-info-2"/></span></p>
          </div>
          <div className="timeline-dot"/>
          <i className="su-icon icon-inbox"/>
          <div className="horz-line"/>
        </div>
        <div className={`su-step confirm-email ${index < 1 ? "" : index === 1 ? "active" : "complete"}`}>
          <div className="horz-line start"/>
          <i className="su-icon icon-protect"/>
          <i className="su-icon icon-checkmark"/>
          <div className="su-timeline-number">2</div>
          <div className="step-label">Confirm Email</div>
          <div className="horz-line end"/>
        </div>
        <div className={`su-step profile ${index < 2 ? "" : index === 2 ? "active" : "complete"}`}>
          <div className="horz-line start"/>
          <i className="su-icon icon-user"/>
          <i className="su-icon icon-checkmark"/>
          <div className="su-timeline-number">3</div>
          <div className="step-label">Profile</div>
          <div className="horz-line end"/>
        </div>
        <div className={`su-step checkout ${index < 3 ? "" : index === 3 ? "active" : "complete"}`}>
          <div className="horz-line start"/>
          <i className="su-icon icon-credit-card"/>
          <i className="su-icon icon-checkmark"/>
          <div className="su-timeline-number">4</div>
          <div className="step-label">Checkout</div>
        </div>
      </div>
      <ReactTooltip effect="solid" place="bottom"/>
    </div>
  )
};

export default Step;
