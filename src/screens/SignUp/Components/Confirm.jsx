import React from 'react';

const Confirm = props => {
  return (
    <div id="wizard-step" className="details-left">
      <div className="details-left signup_section">
        <h2 style={{color: '#222'}}>Check your inbox</h2>
        <p>We just shared a code with you — please enter it below. </p>
        <div id="confirm-form">
          <div className="textfield" style={{width: '40%'}}>
            <input className="text" placeholder="Enter confirmation code" type="text" id="signup_token"/>
          </div>
        </div>
        <div className="why-text">
          <h4 style={{color: '#222'}}>Didn't get the code?</h4>
          <p><a href="#">Click here</a> to resend.</p>
        </div>
        <script className="next-button-template signup-email-confirm" type="text/x-mustache">
          <button id="next" type="submit" className="callout button">Next</button>
        </script>
      </div>
    </div>
  )
};

export default Confirm;
