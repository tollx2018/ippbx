import React from 'react';

const TableHeader = (props) => {
  return (
    <tr>
      <th scope="col" className="col-caller-actions" data-dir="false">
        <i className="font-undo-3"/><span/>
      </th>
      <th scope="col" className="col-caller   sortable" data-dir="down">
        <i className="font-user"/> <span>Contact</span>
      </th>
      <th scope="col" className="col-inputs  " data-dir="false">
        <i className="font-grid"/> <span>Keys</span>
      </th>
      <th scope="col" className="col-message-body   hidden-col" data-dir="false">
        <i className="font-bubble"/> <span>Message</span>
      </th>
      <th scope="col" className="col-source   sortable" data-dir="down">
        <i className="font-newspaper"/> <span>Source</span>
      </th>
      <th scope="col" className="col-source-detail  " data-dir="false">
        <i className="font-bars"/> <span>Session Data</span>
      </th>
      <th scope="col" className="col-csr   hidden-col sortable" data-dir="down">
        <i className="font-user-2"/> <span>Score</span>
      </th>
      <th scope="col" className="col-audio  " data-dir="false">
        <i className="font-play"/> <span>Audio</span>
      </th>
      <th scope="col" className="col-metrics sortable sort_desc" data-dir="down">
        <i className="font-signal"/> <span>Metrics</span>
      </th>
      <th scope="col" className="col-receiving-number   sortable" data-dir="down">
        <i className="font-loop"/> <span>Routing</span>
      </th>
      <th scope="col" className="col-actions" data-dir="false">
        <i className="font-bolt"/> <span>Actions</span>
      </th>
    </tr>
  )
};

export default TableHeader;
