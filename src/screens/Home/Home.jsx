import React, { Component } from "react";
import SubHeader from './SubHeader'
import Table from "./Table";
import Header from "../../components/header/Header";

class Home extends Component {
  render() {
    return (
      <div>
        <Header>
          <SubHeader />
        </Header>
        <Table/>
      </div>
    );
  }
}

export default Home;
