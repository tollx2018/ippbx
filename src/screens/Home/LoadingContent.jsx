import React from 'react';

const LoadingContent = props => {
  return (
    <tr className="calls-loader placeholder-row call call-row wide-call">
      <td className="placeholder-column log-column col-caller-actions  ">
        <div className="placeholder-box-1 shimmer-animation"/>
        <div className="placeholder-box-2 shimmer-animation"/>
        <div className="placeholder-box-3 shimmer-animation"/>
      </td>
      <td className="placeholder-column log-column col-caller sortable">
        <div className="placeholder-box-1 shimmer-animation"/>
        <div className="placeholder-box-2 shimmer-animation"/>
        <div className="placeholder-box-3 shimmer-animation"/>
      </td>
      <td className="placeholder-column log-column col-inputs  ">
        <div className="placeholder-box-1 shimmer-animation"/>
        <div className="placeholder-box-2 shimmer-animation"/>
        <div className="placeholder-box-3 shimmer-animation"/>
      </td>
      <td className="placeholder-column log-column col-message-body   hidden-col">
        <div className="placeholder-box-1 shimmer-animation"/>
        <div className="placeholder-box-2 shimmer-animation"/>
        <div className="placeholder-box-3 shimmer-animation"/>
      </td>
      <td className="placeholder-column log-column col-source   sortable">
        <div className="placeholder-box-1 shimmer-animation"/>
        <div className="placeholder-box-2 shimmer-animation"/>
        <div className="placeholder-box-3 shimmer-animation"/>
      </td>
      <td className="placeholder-column log-column col-source-detail  ">
        <div className="placeholder-box-1 shimmer-animation"/>
        <div className="placeholder-box-2 shimmer-animation"/>
        <div className="placeholder-box-3 shimmer-animation"/>
      </td>
      <td className="placeholder-column log-column col-csr   hidden-col sortable">
        <div className="placeholder-box-1 shimmer-animation"/>
        <div className="placeholder-box-2 shimmer-animation"/>
        <div className="placeholder-box-3 shimmer-animation"/>
      </td>
      <td className="placeholder-column log-column col-audio  ">
        <div className="placeholder-box-1 shimmer-animation"/>
        <div className="placeholder-box-2 shimmer-animation"/>
        <div className="placeholder-box-3 shimmer-animation"/>
      </td>
      <td className="placeholder-column log-column col-metrics   sortable">
        <div className="placeholder-box-1 shimmer-animation"/>
        <div className="placeholder-box-2 shimmer-animation"/>
        <div className="placeholder-box-3 shimmer-animation"/>
      </td>
      <td className="placeholder-column log-column col-receiving-number   sortable">
        <div className="placeholder-box-1 shimmer-animation"/>
        <div className="placeholder-box-2 shimmer-animation"/>
        <div className="placeholder-box-3 shimmer-animation"/>
      </td>
      <td className="placeholder-column log-column col-actions  ">
        <div className="placeholder-box-1 shimmer-animation"/>
        <div className="placeholder-box-2 shimmer-animation"/>
        <div className="placeholder-box-3 shimmer-animation"/>
      </td>
    </tr>
  )
};

export default LoadingContent;
