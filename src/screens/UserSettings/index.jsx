import React, {useState} from "react";
import './index.css';
import SubHeader from "./Components/SubHeader";
import Sidebar from "./Components/Sidabar";
import AgentProfile from "./Components/AgentProfile";
import Info from "./Components/Info";
import Detail from "./Components/Detail";
import Authentication from "./Components/Authentication";
import AgentContact from "./Components/AgentContact";
import MessageResponse from "./Components/MessageResponse";
import Header from "../../components/header/Header";
import InboundVoicemail from "./Components/InboundVoicemail";
import OutboundVoicemail from "./Components/OutboundVoicemail";
import Devices from "./Components/Devices";
import ActivityHistory from "./Components/ActivityHistory";
import Versions from "./Components/Versions";

const UserSettings = props => {
  const [isInfo, setInfo] = useState(false);
  return (
    <div>
      <Header>
        <SubHeader setInfo={setInfo} isInfo={isInfo}/>
      </Header>
      <div id="content-wrapper">
        <div id="content" className="wrapper">
          <div id="body" style={{overflowX: 'hidden'}}>
            <div className="sidenav-container container-fluid">
              <div className="row sidebar-row">
                <Sidebar setInfo={setInfo} isInfo={isInfo}/>
                <div className="col-md-10 pageform">
                  <div className="row">
                    {isInfo && <Info />}
                    <Detail />
                    <Authentication />
                    <AgentProfile />
                    <AgentContact />
                    <MessageResponse />
                    <InboundVoicemail />
                    <OutboundVoicemail />
                    <Devices />
                    <ActivityHistory />
                    <Versions />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
};

export default UserSettings;



