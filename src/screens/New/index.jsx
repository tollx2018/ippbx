import React, {useState} from "react";
import './index.css';
import SubHeader from "./Components/SubHeader";
import Sidebar from "./Components/Sidabar";
import General from "./Components/General";
import Info from "./Components/Info";
import Header from "../../components/header/Header";
import Setup from "./Components/Setup";
import Numbers from "./Components/Numbers";

const UserSettings = props => {
  const [isInfo, setInfo] = useState(true);
  return (
    <div>
      <Header>
        <SubHeader setInfo={setInfo} isInfo={isInfo}/>
      </Header>
      <div id="content-wrapper">
        <div id="content" className="wrapper">
          <div id="body" style={{overflowX: 'hidden'}}>
            <div className="sidenav-container container-fluid">
              <div className="row sidebar-row">
                <Sidebar/>
                <div className="col-md-10 pageform">
                  <div className="row">
                    {isInfo && <Info/>}
                    <General/>
                    <Setup/>
                    <Numbers/>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
)
};

export default UserSettings;



