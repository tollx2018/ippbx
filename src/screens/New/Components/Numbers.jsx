import React from 'react';

const Numbers = props => {
  return (
    <div className="field guide-target-number-vpns" id="target-numbers">
      <h3>Numbers</h3>
      <div className="clear"/>
      <div className="card">
        <div className="field multi-select">
          <label>Use this target number for the selected tracking numbers</label>
          <select
            style={{width: 920, height: 480, position: 'absolute', left: -9999}}
            className="multiple-list" multiple="multiple"
            id="target_number_virtual_phone_number_ids">
            <option value="1581909">(833) 766-8665</option>
            <option value="1581912">(866) 566-8660</option>
            <option value="1584132">(888) 256-7954</option>
            <option value="1584135">(888) 263-5672</option>
            <option value="1584138">(888) 260-2174</option>
            <option value="1584141">(888) 265-5531</option>
            <option value="1590015">(888) 866-6814</option>
          </select>
          <div className="ms-container multiple-list" id="ms-target_number_virtual_phone_number_ids">
            <div className="ms-selectable">
              <input type="text" className="search text" placeholder="Assign Tracking Numbers"/>
              <i className="font-search"/>
              <ul className="ms-list" tabIndex="0">
                <li className="ms-elem-selectable -49-53-56-49-57-48-57 ms-hover">(833) 766-8665</li>
                <li className="ms-elem-selectable -49-53-56-49-57-49-50">(866) 566-8660</li>
                <li className="ms-elem-selectable -49-53-56-49-57-48-57">(833) 766-8665</li>
                <li className="ms-elem-selectable -49-53-56-49-57-49-50">(866) 566-8660</li>
                <li className="ms-elem-selectable -49-53-56-49-57-48-57">(833) 766-8665</li>
                <li className="ms-elem-selectable -49-53-56-49-57-49-50">(866) 566-8660</li>
                <li className="ms-elem-selectable -49-53-56-49-57-48-57">(833) 766-8665</li>
                <li className="ms-elem-selectable -49-53-56-49-57-49-50">(866) 566-8660</li>
                <li className="ms-elem-selectable -49-53-56-49-57-48-57">(833) 766-8665</li>
                <li className="ms-elem-selectable -49-53-56-49-57-49-50">(866) 566-8660</li>
              </ul>
              <p><a className="selectall" href="#">select all</a></p>
            </div>
            <div className="ms-selection">
              <input type="text" className="search text" placeholder="Assigned Tracking Numbers"/>
              <i className="font-search"/>
              <ul className="ms-list" tabIndex="0">
                <li className="ms-elem-selection -49-53-56-49-57-48-57">(833) 766-8665</li>
                <li className="ms-elem-selection -49-53-56-49-57-49-50">(866) 566-8660</li>
                <li className="ms-elem-selection -49-53-56-49-57-48-57">(833) 766-8665</li>
                <li className="ms-elem-selection -49-53-56-49-57-49-50">(866) 566-8660</li>
                <li className="ms-elem-selection -49-53-56-49-57-48-57">(833) 766-8665</li>
                <li className="ms-elem-selection -49-53-56-49-57-49-50">(866) 566-8660</li>
                <li className="ms-elem-selection -49-53-56-49-57-48-57">(833) 766-8665</li>
                <li className="ms-elem-selection -49-53-56-49-57-49-50">(866) 566-8660</li>
                <li className="ms-elem-selection -49-53-56-49-57-48-57">(833) 766-8665</li>
              </ul>
              <p><a className="selectnone" href="#">unselect all</a></p>
            </div>
          </div>
          <div className="clear-break"/>
        </div>
        <footer>
          <button className="button callout save-button guide-save-button">
            <div className="indicator-loop"><i className="save"/></div>
            <span>Save Changes</span></button>
        </footer>
      </div>
    </div>
  )
};

export default Numbers;
