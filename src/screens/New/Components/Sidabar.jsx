import React, {useState} from "react";
import Scrollspy from 'react-scrollspy';
import '../index.css';

const Sidebar = props => {
  const items = [
    'target-general',
    'target-setup',
    'target-numbers'
  ];
  return (
    <div className="col-md-2">
      <nav className="hidden-print hidden-xs hidden-sm pagenav affix">
        <Scrollspy
          items={items}
          offset={-80}
          currentClassName="active"
          className="nav nav-pills nav-stacked"
        >
            <li><a href="#target-general">General</a></li>
            <li><a href="#target-setup">Setup</a></li>
            <li><a href="#target-numbers">Numbers</a></li>
        </Scrollspy>
      </nav>
    </div>
  );
};

export default Sidebar;
