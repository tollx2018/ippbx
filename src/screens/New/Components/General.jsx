import React from 'react';

const General = props => {
  return (
    <div className="field" id="target-general">
      <h3>General
        <small>provide an optional description for this target number</small>
      </h3>
      <div className="clear"/>
      <div className="card">
        <div className="field textfield">
          <label>Description
            <small>(optional)</small>
          </label>
          <input style={{width: 500}} className="text string guide-target-number-description" type="text"/>
          <div className="clear-break"/>
        </div>
        <footer>
          <button className="button callout save-button guide-save-button">
            <div className="indicator-loop"><i className="save"/></div>
            <span>Save Changes</span>
          </button>
        </footer>
      </div>
    </div>
  )
};

export default General;
