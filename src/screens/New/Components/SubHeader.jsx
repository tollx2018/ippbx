import React, {useState} from "react";
import '../index.css';

const SubHeader = props => {
  const {isInfo, setInfo} = props;
  return (
    <div className="header crumbs" style={{marginTop: -40}}>
      <div className="title">
        <div className="paths-md hidden-sm hidden-xs">
          <a className="text link_back index_0" href="#">Target Numbers</a>
          <span className="edge"/>
          <span className="text">
            <span className="select2-choice">
              <span className="result-text">New</span>
            </span>
          </span>
        </div>

        <div className="paths-sm hidden-md hidden-lg">
          <a href="#">
            <i className="font-chevron-left"/>
          </a>
          <span>Edit User</span>
        </div>
      </div>
      <div className="actions">
        <a className="button no-add show-data-help" onClick={() => setInfo(!isInfo)}>
          <i className="font-info"/>&nbsp;
          <span className="caption">
            <span className="hidden-xs">Info</span>
          </span>
          <span className="hidden-sm hidden-md hidden-lg"/>
        </a>
      </div>
    </div>

  );
};

export default SubHeader;
