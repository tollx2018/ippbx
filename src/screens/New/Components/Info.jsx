import React from 'react';

const Info = props => {
  return (
    <div className="field" id="data-help">
      <div className="about-section card">
        <div style={{marginBottom: 10}}>
          <div style={{float: 'left'}}>
            <b>Target Numbers</b> are the numbers we will look for on your website
            and dynamically change/swap to a <a
            href="#">tracking
            number</a>. If you want us to search for multiple numbers on your
            website, you would need to add a target number for each phone number on
            your website. Our <a
            href="#">JavaScript
            code</a> looks for the target numbers on your website and dynamically
            inserts a tracking number in their place (using the same format as the
            target number as it appears on your website).
          </div>
          <div className="clear"/>
          <a className="data-help-button close-data-help">Close</a>
          <a className="data-help-button" target="_blank" href="#">More Info</a>
        </div>
      </div>
    </div>
  )
};

export default Info;
