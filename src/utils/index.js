export const changeTheme = () => {
  const html = document.getElementsByTagName("html")[0];
  if (localStorage.getItem("custom_theme") === "dark") {
    html.className = "dark";
  } else {
    html.className = "";
  }
};
